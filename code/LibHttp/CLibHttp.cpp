﻿// CHttpLib.cpp : 定义静态库的函数。
//
#define  _CRT_SECURE_NO_WARNINGS
#include "pch.h"
#include "CLibHttp.h"
#include "framework.h"
#include "HttpTypeBase.h"
#include "HttpClient.h"
#include "URI.h"
#include "SSLContext.h"
GS g;
// TODO: 这是一个库函数示例
SSLCtx s;
std::string RequestGet(std::string scheme, std::string host, int port, std::string path, std::map<std::string, std::string> param)
{
	HttpClient client;
	std::string sParam;
	for (auto p : param)
	{
		sParam += p.first + "=" + p.second + "&";
	}
	sParam.substr(0, sParam.size() - 1);
	return client.RestGet(scheme, host, port, path, sParam);
}
std::string RequestPost(std::string scheme, std::string host, int port, std::string path, std::map<std::string, std::string> param)
{
	HttpClient client;
	std::string sParam;
	for (auto p : param)
	{
		sParam += p.first + "=" + p.second + "&";
	}
	sParam.substr(0, sParam.size() - 1);
	return client.RestPost(scheme, host, port, path, sParam);
}
std::string RequestGet(std::string base, std::map<std::string, std::string> param)
{
	HttpClient client;
	URI uri(base);
	std::string sParam;
	for (auto p : param)
	{
		sParam += p.first + "=" + p.second + "&";
	}
	sParam.substr(0, sParam.size() - 1);
 	return client.RestGet(uri.GetScheme(), uri.GetHost(), uri.GetPort(), uri.GetPath(), sParam);
}
std::string RequestPost(std::string base, std::map<std::string, std::string> param)
{
	HttpClient client;
	URI uri(base);
	std::string sParam;
	for (auto p : param)
	{
		sParam += p.first + "=" + p.second + "&";
	}
	sParam.substr(0, sParam.size() - 1);
	return client.RestPost(uri.GetScheme(), uri.GetHost(), uri.GetPort(), uri.GetPath(), sParam);

}