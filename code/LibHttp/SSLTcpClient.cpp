#include "BaseInc.h"
#include "SSLTcpClient.h"
SSLTcpClient::SSLTcpClient():TcpClient()
{

}

SSLTcpClient::~SSLTcpClient()
{

}

void SSLTcpClient::Init(string addr, int port)
{
	TcpClient::Init(addr, port);
	const SSL_METHOD* meth = SSLv23_client_method();
	//建立新的SSL上下文 
	ctx = SSL_CTX_new(meth);
	if (ctx == NULL)
	{
		ERR_print_errors_fp(stderr);
		std::cout << "SSL_CTX_new error !" << std::endl;
	}
	ssl = SSL_new(ctx);
	if (ssl == NULL)
	{
		std::cout << "SSL NEW error" << std::endl;
		return;
	}
	
	SSL_set_fd(ssl, clntSock);
	//SSL连接 
	int ret = SSL_connect(ssl);
	if (ret == -1)
	{
		std::cout << "SSL ACCEPT error " << std::endl;
		return;
	}
}

void SSLTcpClient::Send(char* buff, int size)
{
	SSL_write(ssl, buff, size);
}

void SSLTcpClient::Send(ByteData& buff)
{
	int sendSize = SSL_write(ssl, buff.GetData(), buff.size());
	if (sendSize < 0)
	{
		std::cout << "Failed to send data!" << std::endl;
	}
	else {
		//std::cout << "Send data:" << sendSize << std::endl;
	}
}

void SSLTcpClient::Receive(char* buff, int size)
{
	int ret = SSL_read(ssl, buff, size);
	if (ret < 0) {
		std::cout << "Failed to receive data!" << std::endl;
	}
	else {
		//cout << buff << endl;
	}
}

void SSLTcpClient::ReceiveAll(ByteData& buff)
{
	char ch[1024];
	memset(ch, 0, 1024);
	while (true)
	{
		int ret = SSL_read(ssl, ch, 1024);
		if (ret <= 0) {
			std::cout << "Failed to receive data!" << std::endl;
			nConnect = 0;
			break;
		}
		else {
			//cout << buff << endl;
			for (int i = 0; i < ret; i++)
			{
				buff.push_back(ch[i]);
			}
			memset(ch, 0, 1024);
			break;
		}
	}
}

void SSLTcpClient::Close()
{
	//关闭SSL套接字 
	SSL_shutdown(ssl);
	//释放SSL套接字 
	SSL_free(ssl);
	//释放SSL会话环境 
	SSL_CTX_free(ctx);
	TcpClient::Close();
}

void SSLTcpClient::ReceiveThread()
{

}

