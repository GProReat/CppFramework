#include "BaseInc.h"
#include "URI.h"
URI::URI() {
	SetHttpScheme();
	SetHost("localhost");
	SetPort("80");
	SetPath("/");
	BuildUri();
}
URI::URI(std::string uri)
{
	this->uri = uri;
	Parse();
}

void URI::SetStringUri(std::string uri)
{
	this->uri = uri;
	Parse();
}

void URI::SetHttpScheme()
{
	scheme = "http";
}

void URI::SetHttpsScheme()
{
	scheme = "https";
}

void URI::SetPort(std::string port)
{
	this->port = port;
}

void URI::SetHost(std::string host)
{
	this->host = host;
}

void URI::SetPath(std::string path)
{
	this->path = path;
}

void URI::SetPara(std::string para, std::string val)
{
	mPara[para] = val;
}

std::string URI::GetPara(std::string para)
{
	return	mPara[para];
}

std::string URI::GetStringUri()
{
	return uri;
}

int URI::GetPort()
{
	if (port == "")
	{
		if (scheme == "http")
			port = "80";
		if (scheme == "https")
			port = "443";
	}
	return atoi(port.data());
}

std::string URI::GetHost()
{
	return host;
}

std::string URI::GetScheme()
{
	return scheme;
}

std::string URI::GetPath()
{
	return path;
}

void URI::BuildUri()
{
	std::string url;
	bool bIgnorePort = false;
	if (scheme == "http" && port == "80")
		bIgnorePort = true;
	if (scheme == "https" && port == "443")
		bIgnorePort = true;
	std::string tmpPath = path;
	int npos = tmpPath.find('/');
	while (npos == 0)
	{
		tmpPath = tmpPath.substr(npos + 1);
		npos = tmpPath.find('/');
	}

	if (bIgnorePort)
	{
		url = scheme + "://" + host + "/" + tmpPath;
	}
	else {
		url = scheme + "://" + host + ":" + port + "/" + tmpPath;
	}
	if (mPara.size() != 0)
	{
		std::string paraStr;
		for (auto it : mPara)
		{
			paraStr += it.first + "=" + it.second + "&";
		}
		paraStr = paraStr.substr(0, paraStr.size() - 1);
		url = url + "?" + paraStr;
	}
	uri = url;
}

void URI::Parse()
{
	size_t pos = uri.find("http://");
	if (pos == 0)
	{
		scheme = "http";
		uri = uri.substr(7);
	}
	pos = uri.find("https://");
	if (pos == 0)
	{
		scheme = "https";
		uri = uri.substr(8);
	}
	pos = uri.find('/');
	string hostportStr = uri.substr(0, pos);
	int hp = hostportStr.find(':');
	if (hp == string::npos)
	{
		host = hostportStr;
	}
	else {
		host = hostportStr.substr(0, hp);
		port = hostportStr.substr(hp + 1, pos);
	}
	//只剩下path + para
	uri = uri.substr(pos);
	pos = uri.find('?');
	if (pos == std::string::npos)
	{
		path = uri;
		//没有para
		return;
	}
	path = uri.substr(0, pos);
	std::string paraStr = uri.substr(pos + 1);
	paraStr.push_back('&');
	pos = paraStr.find('&');
	std::string var, val;
	while (pos != -1)
	{
		std::string tmp = paraStr.substr(0, pos);
		//处理tmp
		ParseExp(tmp, std::move(var), std::move(val));
		mPara[var] = val;
		paraStr = paraStr.substr(pos + 1);
		pos = paraStr.find('&');
	}
}

void URI::ParseExp(std::string exp, std::string&& var, std::string&& val)
{
	int pos = exp.find('=');
	var = exp.substr(0, pos);
	val = exp.substr(pos + 1);
}