#pragma once
#include "BaseInc.h"
#include "ByteData.h"
class IHttpStreamBase
{
public:
	string Name;
	virtual ~IHttpStreamBase() {}

	virtual void Write(ByteData data) {
		m_byteData.WriteData(data);
	}
	virtual void Write(string data) {
		m_byteData.WriteString(data);
	}
	virtual string ReadString() = 0;
	// ascii utf8 unicode 
	//compress
protected:
	ByteData m_byteData;

};
class AsciiSteam : public IHttpStreamBase
{
public:
	AsciiSteam() {
		Name = "ascii";
	}
	virtual string ReadString();
};
class GbkSteam : public IHttpStreamBase
{
public:
	GbkSteam() {
		Name = "gbk";
	}
	virtual string ReadString();
};
class UnicodeStream : public IHttpStreamBase {
public:
	UnicodeStream() {
		Name = "unicode";
	}
	//默认源数据就是unicode
	virtual string ReadString();
};
class Utf8Stream : public IHttpStreamBase {
public:
	Utf8Stream() : IHttpStreamBase() {
		Name = "utf-8";
	}
	~Utf8Stream() {}
	//默认源数据就是unicode/utf8
	virtual string ReadString();
};



