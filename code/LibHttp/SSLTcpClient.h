#pragma once
#include "BaseInc.h"
#include "ByteData.h"
#include "TcpClient.h"
#include "SSLContext.h"

class SSLTcpClient : public TcpClient
{
public:
	SSLTcpClient();
	~SSLTcpClient();
	virtual void Init(string addr, int port);
	void Send(char* buff, int size);
	void Send(ByteData& buff);
	void Receive(char* buff, int size);
	//继续
	void ReceiveAll(ByteData& buff);
	void Close();

	SOCKET servSock;
	SOCKET clntSock;
private:
	void ReceiveThread();
	bool isRecvThreadRunning;
	int nConnect;	//< -1 未初始化 0 未连接 1 连接
	SSL_CTX* ctx;
	SSL* ssl;
};
