#pragma once
#define  _CRT_SECURE_NO_WARNINGS
#include "BaseInc.h"
#include "HttpTypeBase.h"
#include "HttpStream.h"
#include "ByteData.h"
class Response
{
public:
	Response() {}
	~Response() {}
	void LoadResponseData(ByteData& data)
	{
		m_byteData.WriteData(data);
		ParseHeader();
	}
	string& GetContent()
	{
		return content;
	}
	Header& GetHeader() { return header; }
protected:
	void ParseHeader()
	{
		stringstream ss;
		ss.write(m_byteData.GetData(), m_byteData.size());
		string sLine;
		string sHeader;
		string sVal;
		stringstream lineSS;
		while (!ss.eof())
		{
			getline(ss, sLine);
			if (sLine != "\r")
			{
				if (sLine.find(':') == std::string::npos)
				{
					lineSS.clear();
					lineSS.str("");
					continue;
				}
				lineSS << sLine;
				getline(lineSS, sHeader, ':');
				getline(lineSS, sVal, ':');
				auto it = std::remove(sVal.begin(), sVal.end(), ' ');
				sVal.erase(it);
				it = std::remove(sVal.begin(), sVal.end(), '\r');
				sVal.erase(it);
				it = std::remove(sVal.begin(), sVal.end(), '\n');
				sVal.erase(it);
				header[sHeader] = sVal;
				lineSS.clear();
				lineSS.str("");
			}
			else
			{
				break;
			}
		}
		string slen;
		if (header.find("Content-Length") != header.end())
		{
			slen = header["Content-Length"];
		}
		else {
			while (!IsHex(slen))
			{
				getline(ss, slen);
				auto it = std::remove(slen.begin(), slen.end(), '\r');
				slen.erase(it);
			}
		}

		int len;
		sscanf_s(slen.data(), "%x", &len);
		IHttpStreamBase* stream = nullptr;

		//现在要根据具体编码进行解析
		if (header["Content-Encoding"] == "gbk")
		{
			stream = new GbkSteam();
		}if (header["Content-Encoding"] == "ascii")
		{
			stream = new UnicodeStream();
		}
		if (header["Content-Encoding"] == "utf-8" || header["Content-Encoding"] == "utf8")
		{
			stream = new Utf8Stream();
		}
		if (header["Content-Encoding"] == "unicode")
		{
			stream = new UnicodeStream();
		}
		if (stream == nullptr)
		{
			stream = new GbkSteam();
		}
		ByteData bdata;
		char ch[1024];
		while (!ss.eof())
		{
			ss.read(ch, 1024);
			int nReadNum = ss.gcount();
			bdata.Write(ch, nReadNum);
		}
		stream->Write(bdata);
		//为了正确统计字符的数量和正确切割字符串
		content = stream->ReadString();
		wstring wsrContent = StringToWString(content);
		wsrContent = wsrContent.substr(0, len);
		content = WStringToString(wsrContent);
		delete stream;
	}

	Header header;
	//任何编码 都被转换为ascii
	string content;
	ByteData m_byteData;

};