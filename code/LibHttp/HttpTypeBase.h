#pragma once
#include "BaseInc.h"
class GS
{
public:
	GS() {
		WORD winsock_version = MAKEWORD(2, 2);
		WSADATA wsa_data;
		if (WSAStartup(winsock_version, &wsa_data) != 0) {
			std::cout << "Failed to init socket dll!" << std::endl;
		}
	}
	~GS() {
		WSACleanup();
	}
};

bool IsHex(string str);

std::wstring Utf82Unicode(const std::string& utf8string);
//unicode תΪ ascii  
std::wstring Acsi2WideByte(std::string& strascii);
std::string Unicode2Utf8(const std::wstring& widestring);
std::string WideByte2Acsi(std::wstring& wstrcode);
std::string UTF_82ASCII(std::string& strUtf8Code);
std::string ASCII2UTF_8(std::string& strAsciiCode);
std::string UnicodeToAscii(wchar_t* src); 
std::wstring AsciiToUnicode(char* src);

std::string WStringToString(const std::wstring src);
std::wstring StringToWString(const std::string src);