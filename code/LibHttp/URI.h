#pragma once
#include <map>
#include <string>

struct URI
{
	URI();
	URI(std::string uri);
	void SetStringUri(std::string uri);
	void SetHttpScheme();
	void SetHttpsScheme();
	void SetPort(std::string port);
	void SetHost(std::string host);
	void SetPath(std::string path);
	void SetPara(std::string para, std::string val);
	/**
	 * 设置完相关参数后，需要buildUri
	 */
	void BuildUri();

	std::string GetStringUri();
	std::string GetScheme();
	std::string GetHost();
	int GetPort();
	std::string GetPath();
	std::string GetPara(std::string para);

private:
	void Parse();
	void ParseExp(std::string exp, std::string&& var, std::string&& val);
private:
	std::string host;
	std::string port;
	std::string path;
	std::string scheme;
	std::map<std::string, std::string> mPara;
	std::string uri;
};
