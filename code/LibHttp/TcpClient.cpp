#include "BaseInc.h"
#include "TcpClient.h"
#include "ByteData.h"

TcpClient::TcpClient() :nConnect(-1)
{
}

void TcpClient::Init(string addr, int port)
{
	clntSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (clntSock == INVALID_SOCKET) {
		//std::cout << "Failed to create server socket!" << std::endl;
		nConnect = -1;
		return;
	}
	// 绑定IP和端口  及需要连接的目的IP，和端口
	sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.S_un.S_addr = inet_addr(addr.data());

	// connect() 向服务器发起请求，服务器的IP地址和端口号保存在 sockaddr_in 结构体中。直到服务器传回数据后，connect() 才运行结束。
	if (connect(clntSock, (LPSOCKADDR)&server_addr, sizeof(server_addr)) == SOCKET_ERROR) {
		//std::cout << "Failed to connect server!" << std::endl;
		nConnect = 0;
	}
	else {
		nConnect = 1;
	}
}

void TcpClient::StartReceiveThread()
{
	thread th(&TcpClient::ReceiveThread, this);
	th.detach();
}

void TcpClient::Send(char* buff, int size)
{
	if (send(clntSock, buff, size, 0) < 0)
	{
		std::cout << "Failed to send data!" << std::endl;
	}
}

void TcpClient::Send(ByteData& buff)
{
	int sendSize = send(clntSock, buff.GetData(), buff.size(), 0);
	if (sendSize < 0)
	{
		std::cout << "Failed to send data!" << std::endl;
	}
	else {
		//std::cout << "Send data:" << sendSize << std::endl;
	}
}

void TcpClient::Receive(char* buff, int size)
{
	int ret = recv(clntSock, buff, size, 0);
	if (ret < 0) {
		std::cout << "Failed to receive data!" << std::endl;
	}
	else {
		//cout << buff << endl;
	}
}

void TcpClient::ReceiveAll(ByteData& buff)
{
	char ch[1024];
	memset(ch, 0, 1024);
	while (true)
	{
		int ret = recv(clntSock, ch, 1024, 0);
		if (ret <= 0) {
			std::cout << "Failed to receive data!" << std::endl;
			nConnect = 0;
			break;
		}
		else {
			//cout << buff << endl;
			for (int i = 0; i < ret; i++)
			{
				buff.push_back(ch[i]);
			}
			memset(ch, 0, 1024);
			break;
		}
	}

}

void TcpClient::ReceiveThread()
{
	char recvBuff[RecvSize];
	isRecvThreadRunning = true;
	while (isRecvThreadRunning)
	{
		int ret = recv(clntSock, recvBuff, RecvSize, 0);
		if (ret > 0) {
			cout << recvBuff << endl;
			memset(recvBuff, 0, RecvSize);
		}
	}
}

void TcpClient::Close()
{
	isRecvThreadRunning = false;
	closesocket(clntSock);
	nConnect = -1;
}

