#pragma once
#include <iostream>
#include <map>
#include <string>

/**
 * 使用get做http请求
 * @scheme 请求前缀 http 或者https
 * @host 服务器ip地址 （域名需要单独解析）
 * @port 端口
 * @param 参数请求
 * @return 返回response body字符串
 */
 std::string RequestGet(std::string scheme, std::string host, int port, std::string path,std::map<std::string,std::string> param);
/**
 * 使用post做http请求
 * @scheme 请求前缀 http 或者https
 * @host 服务器ip地址 （域名需要单独解析）
 * @port 端口
 * @param 参数请求
 * @return 返回response body字符串
 */
 std::string RequestPost(std::string scheme, std::string host, int port, std::string path, std::map<std::string, std::string> param);
 /**
 * 使用get做http请求
 * @base url
 * @param 请求参数
 * @return 返回response body字符串
 */
 std::string RequestGet(std::string base, std::map<std::string, std::string> param);
 /**
 * 使用get做http请求
 * @base url
 * @param 请求参数
 * @return 返回response body字符串
 */
 std::string RequestPost(std::string base, std::map<std::string, std::string> param);
