#pragma once
#include"BaseInc.h"
#include "TcpClient.h"
#include "Request.h"
#include "Response.h"
class HttpClient
{
public:
	string RestGet(string scheme, string host, int port, string base, string param);
	/**
	 * 使用post做http请求
	 * @host 服务器ip地址 （域名需要单独解析）
	 * @port 端口
	 * @param 参数请求
	 * @return 返回response body字符串
	 */
	string RestPost(string scheme, string host, int port, string base, string param);

	/**
	 * 通过主机名获取ip地址
	 */

	std::string getHostByHostName(std::string hostName);


private:
	TcpClient* clnt;
	TcpClient client;
	TcpClient sslclient;

};