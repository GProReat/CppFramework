#pragma once
#include "BaseInc.h"
#include "ByteData.h"
class TcpClient
{
public:
	TcpClient();
	virtual void Init(string addr, int port);
	void StartReceiveThread();
	virtual void Send(char* buff, int size);
	virtual void Send( ByteData& buff);
	virtual void Receive(char* buff, int size);
	bool IsConnect() {
		return 	nConnect > 0;
	}
	//继续
	virtual void ReceiveAll(ByteData& buff);
	virtual void Close();

	SOCKET servSock;
	SOCKET clntSock;
	int port;
	string addr;
	bool GetIsRunning() { return isRecvThreadRunning; }
private:
	virtual void ReceiveThread();
	bool isRecvThreadRunning;
	int nConnect;	//< -1 未初始化 0 未连接 1 连接
};