#pragma once

#include <WINSOCK2.H> 
#include <iostream>
#include <sstream>

#pragma comment(lib,"libssl.lib")
#pragma comment(lib,"libcrypto.lib")
#pragma comment(lib,"ws2_32.lib")
struct SSLCtx
{
	SSLCtx()
	{
		//加载SSL错误信息 
		SSL_load_error_strings();
		//添加SSL的加密/HASH算法 
		SSLeay_add_ssl_algorithms();
	}
};

struct HttpsRequest {
	HttpsRequest() {
		//加载SSL错误信息 
		SSL_load_error_strings();
		//添加SSL的加密/HASH算法 
		SSLeay_add_ssl_algorithms();
		const SSL_METHOD* meth = SSLv23_client_method();
		//建立新的SSL上下文 
		ctx = SSL_CTX_new(meth);
		if (ctx == NULL)
		{
			ERR_print_errors_fp(stderr);
			std::cout << "SSL_CTX_new error !"<<std::endl;
		}
		ssl = SSL_new(ctx);
		if (ssl == NULL)
		{
			std::cout << "SSL NEW error" << std::endl;
			return;
		}
	}
	~HttpsRequest()
	{
		//关闭SSL套接字 
		SSL_shutdown(ssl);
		//释放SSL套接字 
		SSL_free(ssl);
		//释放SSL会话环境 
		SSL_CTX_free(ctx);
	}
	void BindSocket(SOCKET client)
	{
		SSL_set_fd(ssl, client);
		//SSL连接 
		int ret = SSL_connect(ssl);
		if (ret == -1)
		{
			std::cout << "SSL ACCEPT error "<<std::endl;
			return ;
		}
	}
	void Write(char* data, int len)
	{
		SSL_write(ssl, data, len);
	}
	void Read(char* data,int len)
	{
		/*while ((ret = SSL_read(ssl, rec + start, 1024)) > 0)
		{
			start += ret;
		}*/
	}

private:
	SSL_CTX* ctx;
	SSL* ssl;

};