#include "BaseInc.h"
#include "HttpStream.h"
#include "HttpTypeBase.h"


std::string GbkSteam::ReadString()
{
	string s;
	int seekPos = 0;
	auto begin = s.begin();
	for (int i = 0; i < m_byteData.size(); i++)
	{
		if (m_byteData[i] == 0)
		{
			seekPos = i;
			break;
		}
		else {
			s.push_back(m_byteData[i]);
		}
	}
	return s;
}

string UnicodeStream::ReadString() {
	string s;
	wstring wsr;
	int seekPos = 0;
	auto begin = s.begin();
	for (int i = 0; i < m_byteData.size(); i++)
	{
		if (m_byteData[i] == 0)
		{
			seekPos = i;
			break;
		}
		else {
			s.push_back(m_byteData[i]);
		}
	}
	m_byteData.RemoveFront(seekPos);
	return UTF_82ASCII(s);
	//return UnicodeToAscii(const_cast<wchar_t*>(wsr.data()));
}
std::string Utf8Stream::ReadString()
{
	string s;
	int seekPos = 0;
	auto begin = s.begin();
	for (int i = 0; i < m_byteData.size(); i++)
	{
		if (m_byteData[i] == 0)
		{
			seekPos = i;
			break;
		}
		else {
			s.push_back(m_byteData[i]);
			//	s.erase(begin + i);
		}
	}
	//
	m_byteData.RemoveFront(seekPos + 1);
	return UTF_82ASCII(s);
}