#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <functional>
#include <utility>
#include <stdlib.h>
#include <thread>
#include <WinSock2.h>
#include <vector>
#include <map>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <functional>
#include <math.h>
#include <windows.h>
#include "openssl/ssl.h"
#include "openssl/err.h"

#pragma comment(lib,"libssl.lib")
#pragma comment(lib,"libcrypto.lib")
#pragma comment(lib,"WS2_32.lib")

using namespace std;
#define  RecvSize 1024
#define  SendSize 1024


//typedef std::vector<char> ByteBuff;
//
//class ByteData
//{
//public:
//	std::vector<char> data;
//public:
//	ByteData() {}
//	void WriteData(ByteData& bdt)
//	{
//		for (int i = 0; i < bdt.data.size(); i++)
//		{
//			data.push_back(bdt.data[i]);
//		}
//	}
//	void push_back(char ch) {
//		data.push_back(ch);
//	}
//	char operator[](int index)
//	{
//		return data[index];
//	}
//	char* GetData() {
//		if (data.size() == 0)
//			return nullptr;
//		return &data[0];
//	}
//	void clear() {
//		data.clear();
//	}
//	size_t size() {
//		return data.size();
//	}
//	void print()
//	{
//		if (data.size() == 0)
//			return;
//		cout << GetData() << endl;
//	}
//	void WriteString(string str)
//	{
//		for (int i = 0; i < str.size(); i++)
//		{
//			data.push_back(str[i]);
//		}
//		data.push_back(0);
//	}
//};

using Header = std::map<string, string>;
