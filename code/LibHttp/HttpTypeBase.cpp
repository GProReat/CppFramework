#include "BaseInc.h"
#include "HttpTypeBase.h"

bool IsHex(string str)
{
	if (str.empty()) return false;
	int* v = new int[str.size()];
	memset(v, 0, str.size());
	char hexCh[] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','a','b','c','d','e','f' };
	for (int i = 0; i < str.size(); i++)
	{
		for (int h = 0; h < sizeof(hexCh); h++)
		{
			char ch = str[i];
			if (str[i] == hexCh[h])
			{
				v[i] = h;
				break;
			}
		}
		if (v[i] < 0)
		{
			delete[] v;
			return false;
		}
	}
	delete[] v;
	return true;
}

std::wstring Utf82Unicode(const std::string& utf8string)
{
	int widesize = ::MultiByteToWideChar(CP_UTF8, 0, utf8string.c_str(), -1, NULL, 0);
	if (widesize == ERROR_NO_UNICODE_TRANSLATION)
	{
		throw std::exception("Invalid UTF-8 sequence.");
	}
	if (widesize == 0)
	{
		throw std::exception("Error in conversion.");
	}

	std::vector<wchar_t> resultstring(widesize);

	int convresult = ::MultiByteToWideChar(CP_UTF8, 0, utf8string.c_str(), -1, &resultstring[0], widesize);

	if (convresult != widesize)
	{
		throw std::exception("La falla!");
	}

	return std::wstring(&resultstring[0]);
}

std::wstring Acsi2WideByte(std::string& strascii)
{
	int widesize = MultiByteToWideChar(CP_ACP, 0, (char*)strascii.c_str(), -1, NULL, 0);
	if (widesize == ERROR_NO_UNICODE_TRANSLATION)
	{
		throw std::exception("Invalid UTF-8 sequence.");
	}
	if (widesize == 0)
	{
		throw std::exception("Error in conversion.");
	}
	std::vector<wchar_t> resultstring(widesize);
	int convresult = MultiByteToWideChar(CP_ACP, 0, (char*)strascii.c_str(), -1, &resultstring[0], widesize);


	if (convresult != widesize)
	{
		throw std::exception("La falla!");
	}

	return std::wstring(&resultstring[0]);
}

std::string Unicode2Utf8(const std::wstring& widestring)
{
	int utf8size = ::WideCharToMultiByte(CP_UTF8, 0, widestring.c_str(), -1, NULL, 0, NULL, NULL);
	if (utf8size == 0)
	{
		throw std::exception("Error in conversion.");
	}

	std::vector<char> resultstring(utf8size);

	int convresult = ::WideCharToMultiByte(CP_UTF8, 0, widestring.c_str(), -1, &resultstring[0], utf8size, NULL, NULL);

	if (convresult != utf8size)
	{
		throw std::exception("La falla!");
	}

	return std::string(&resultstring[0]);
}

std::string WideByte2Acsi(std::wstring& wstrcode)
{
	int asciisize = ::WideCharToMultiByte(CP_OEMCP, 0, wstrcode.c_str(), -1, NULL, 0, NULL, NULL);
	if (asciisize == ERROR_NO_UNICODE_TRANSLATION)
	{
		throw std::exception("Invalid UTF-8 sequence.");
	}
	if (asciisize == 0)
	{
		throw std::exception("Error in conversion.");
	}
	std::vector<char> resultstring(asciisize);
	int convresult = ::WideCharToMultiByte(CP_OEMCP, 0, wstrcode.c_str(), -1, &resultstring[0], asciisize, NULL, NULL);

	if (convresult != asciisize)
	{
		throw std::exception("La falla!");
	}

	return std::string(&resultstring[0]);
}

std::string UTF_82ASCII(std::string& strUtf8Code)
{
	std::string strRet("");
	//先把 utf8 转为 unicode  
	std::wstring wstr = Utf82Unicode(strUtf8Code);
	//最后把 unicode 转为 ascii  
	strRet = WideByte2Acsi(wstr);
	return strRet;
}

std::string ASCII2UTF_8(std::string& strAsciiCode)
{
	std::string strRet("");
	//先把 ascii 转为 unicode  
	std::wstring wstr = Acsi2WideByte(strAsciiCode);
	//最后把 unicode 转为 utf8  
	strRet = Unicode2Utf8(wstr);
	return strRet;
}

std::string UnicodeToAscii(wchar_t* src)
{
	char buff[4096];
	memset(buff, 0, sizeof(char));
	std::string s;
	size_t nCvtCnt;
	int erno = wcstombs_s(&nCvtCnt, buff, 4096, src, 4096);
	s = buff;
	return std::move(s);
}

std::wstring AsciiToUnicode(char* src)
{
	wchar_t buff[4096];
	memset(buff, 0, sizeof(wchar_t));
	std::wstring s;
	size_t nCvtCnt;
	int erno = mbstowcs_s(&nCvtCnt, buff, 4096, src, 4096);
	s = buff;
	return std::move(buff);
}
std::string WStringToString(const std::wstring src)
{
	//std::string result;
	////获取缓冲区大小，并申请空间，缓冲区大小事按字节计算的  
	//int len = WideCharToMultiByte(CP_ACP, 0, src.c_str(), src.size(), NULL, 0, NULL, NULL);
	//char* buffer = new char[len + 1];
	////宽字节编码转换成多字节编码  
	//WideCharToMultiByte(CP_ACP, 0, src.c_str(), src.size(), buffer, len, NULL, NULL);
	//buffer[len] = '\0';
	////删除缓冲区并返回值  
	//result.append(buffer);
	//delete[] buffer;
	const int size = 4096;//wstr.size();
	char ch[size];
	size_t nCvtCnt;
	//wcstombs_s(&nCvtCnt, ch, wstr.data(), 4096);
	int erno = wcstombs_s(&nCvtCnt, ch, size, src.data(), 4096);
	return std::string(ch);
}

std::wstring StringToWString(const std::string src)
{
	wchar_t buff[4096];
	memset(buff, 0, sizeof(wchar_t));
	std::wstring s;
	size_t nCvtCnt;
	int erno = mbstowcs_s(&nCvtCnt, buff, 4096, src.data(), 4096);
	s = buff;
	return std::move(buff);
}
