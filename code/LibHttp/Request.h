#pragma once
#include "BaseInc.h"
#include "TcpClient.h"
class Request
{
public:
	Request() { client = nullptr; }
	void SetClient(TcpClient* _client) { client = _client; }
	void BuildGetRequest(string host, int port, string path, string param)
	{
		ByteData sendBuff;
		stringstream ss;
		if (param == "")
		{
			ss << "GET " << path << " HTTP/1.1" << endl;
		}
		else {
			ss << "GET " << path << "?" << param << " HTTP/1.1" << endl;
		}
		ss << "Host: " << host << ":" << port << endl;
		MakeHeader();
		for (auto it = header.begin(); it != header.end(); it++)
		{
			ss << it->first << ": " << it->second << endl;
		}
		ss << endl;

		sendBuff.WriteString(ss.str());
		client->Send(sendBuff);
	}
	void BuildPostRequest(string host, int port, string path, string param)
	{
		ByteData sendBuff;
		stringstream ss;
		ss << "POST " << path << " HTTP/1.1" << endl;
		ss << "Host: " << host << ":" << port << endl;
		MakeHeader();
		for (auto it = header.begin(); it != header.end(); it++)
		{
			ss << it->first << ": " << it->second << endl;
		}
		ss << "Content-Length: " << param.size() << endl;
		ss << endl;
		ss << param;
		sendBuff.WriteString(ss.str());
 		client->Send(sendBuff);
	}


protected:
	void MakeHeader()
	{
		header.clear();
		header["Connection"] = "keep-alive";
		header["Upgrade-Insecure-Requests"] = "1";
		header["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";
		header["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
		header["Accept-Language"] = "zh-CN,zh;q=0.9";
		header["Cache-Control"] = "no-cache";
	}

	Header header;
	string paraStr;
	TcpClient* client;
};
