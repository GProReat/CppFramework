#include "TimeUtility.h"
#include "boost/date_time.hpp"
using namespace std;
using namespace chrono;
std::string GetCurrentSystemTime()
{
	auto tt = system_clock::to_time_t(system_clock::now());
	tm ptm;
	localtime_s(&ptm, &tt);
	char date[60] = { 0 };

	sprintf_s(date, "%d/%02d/%02d %02d:%02d:%02d:%02d",
		(int)ptm.tm_year + 1900, (int)ptm.tm_mon + 1, (int)ptm.tm_mday,
		(int)ptm.tm_hour, (int)ptm.tm_min, (int)ptm.tm_sec, (int)ptm.tm_wday);
	return std::string(date);
}

std::string Now()
{
	/*auto tt = system_clock::to_time_t(system_clock::now());
	tm ptm;
	localtime_s(&ptm, &tt);
	char date[60] = { 0 };

	std::string ss = "yyyy";*/
	/*sprintf_s(date, "%d/%02d/%02d %02d:%02d:%02d:%02d",
		(int)ptm.tm_year + 1900, (int)ptm.tm_mon + 1, (int)ptm.tm_mday,
		(int)ptm.tm_hour, (int)ptm.tm_min, (int)ptm.tm_sec, (int)ptm.tm_wday);*/
	//need implement
	const boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
	// Get the time offset in current day
	const boost::posix_time::time_duration td = now.time_of_day();

	//frame.h = td.hours();
	//frame.m = td.minutes();
	//frame.s = td.seconds();
	//frame.ms = td.total_milliseconds() - ((frame.h * 3600 + frame.m * 60 + frame.s) * 1000);
	//frame.h = td.hours();
	//frame.m = td.minutes();
	//frame.s = td.seconds();
	//frame.ms = td.total_milliseconds() - ((frame.h * 3600 + frame.m * 60 + frame.s) * 1000);
	return "";
}
