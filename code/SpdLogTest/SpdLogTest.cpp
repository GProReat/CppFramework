﻿// SpdLogTest.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include "boost/filesystem.hpp"
#include "spdlog/include/spdlog/spdlog.h"
#include "spdlog/include/spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/sink.h"

using namespace spdlog;
using namespace spdlog::level;

int base_log()
{
	spdlog::info("Welcome to spdlog!");
	spdlog::error("Some error message with arg: {}", 1);

	spdlog::warn("Easy padding in numbers like {:08d}", 12);
	spdlog::critical("Support for int: {0:d};  hex: {0:x};  oct: {0:o}; bin: {0:b}", 42);
	spdlog::info("Support for floats {:03.2f}", 1.23456);
	spdlog::info("Positional args are {1} {0}..", "too", "supported");
	spdlog::info("{:<30}", "left aligned");

	spdlog::set_level(spdlog::level::debug); // Set global log level to debug
	spdlog::debug("This message should be displayed..");

	// change log pattern
	spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] [thread %t] %v");

	// Compile time log levels
	// define SPDLOG_ACTIVE_LEVEL to desired level
	SPDLOG_TRACE("Some trace message with param {}", {});
	SPDLOG_DEBUG("Some debug message");

	// Set the default logger to file logger
	auto file_logger = spdlog::basic_logger_mt("basic_logger", "logs/basic.txt");
	spdlog::set_default_logger(file_logger);
	return 0;
}

void fileter_log()
{
	try
	{
		auto my_logger = spdlog::basic_logger_mt("basic_logger", "logs/basic-log.txt");

	}
	catch (const spdlog::spdlog_ex &ex)
	{
		std::cout << "Log init failed: " << ex.what() << std::endl;
	}
}
using namespace spdlog::level;
void RotateTest()
{
	boost::filesystem::path  pathObj("logs");
	if (!boost::filesystem::exists(pathObj))
	{
		boost::filesystem::create_directory(pathObj);
	}

	// Create a file rotating logger with 5mb size max and 3 rotated files
	auto rotating_logger = spdlog::rotating_logger_mt("some_logger_name", "logs/rotating.txt", 15000, 5);
	for (int i = 0; i < 50000;i++)
	{
		rotating_logger->info("info的log{}",i);
	}
	rotating_logger->flush();
}
#include "TimeUtility.h"
int main()
{
	base_log();
	//RotateTest();
	//std::cout << GetCurrentSystemTime() << std::endl;
	std::tm t = details::os::localtime(log_clock::to_time_t(std::chrono::system_clock::now()));
	

	return 0;
}
