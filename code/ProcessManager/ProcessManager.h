#pragma once

#include "resource.h"
// Windows 头文件
#include <windows.h>
// C 运行时头文件
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <vector>
#include<string>
#include <Psapi.h>
#include <iostream>
#pragma comment(lib,"psapi.lib")

namespace ProcessManager
{
	BOOL IsProcessExits(const char* str);

	BOOL GetProcessNameById(DWORD prcessId, char* name);

	BOOL StartProcess(std::string processName,std::string arg);

	std::vector<std::string> GetAllProcess();

};
