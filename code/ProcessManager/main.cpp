#include "ProcessManager.h"
#include <algorithm>
#include <varargs.h>
void Replace(std::string& src, const std::string& old, const std::string& newStr)
{
	size_t pos = src.find(old);
	while (pos != std::string::npos)
	{
		src.replace(pos, old.size(), newStr);
		pos = src.find(old, pos + newStr.size());
	}
}
void Remove(std::string& src, const std::string& old, const std::string& newStr)
{
	size_t pos = src.find(old);
	while (pos != std::string::npos)
	{
		src.replace(pos, old.size(), newStr);
		pos = src.find(old, pos + newStr.size());
	}
}
void Trim(std::string& src, const std::string& trimStr)
{
	size_t pos = src.rfind(trimStr);
	if (pos != std::string::npos)
	{
		src.erase(src.begin() + pos - trimStr.size(), src.end());
		src.shrink_to_fit();
	}
}
void TrimAll(std::string& src, const std::string& trimStr)
{
	size_t pos = 0;
	pos = src.rfind(trimStr);
	while (pos == src.size() - trimStr.size())
	{
		src.erase(src.begin() + pos, src.end());
		src.shrink_to_fit();
		//pos = src.find_last_of(trimStr);
		pos = src.rfind(trimStr);
	}
}
int findSubStringCount(std::string& src, const std::string&& ss)
{
	int match = 0;
	size_t pos = src.rfind(ss, 0);
	while (pos != std::string::npos)
	{
		match++;
		pos = src.find(ss, pos + 1);
	}
	return match;
}
//支持%s
void format(std::string& src, ...)
{


	/*va_list args;
	va_start(args, count);
	for (int i = 0; i < count; ++i)
	{
		int arg = va_arg(args, int);
		printf("arg %d = %d", i, arg);
	}
	va_end(args);
	size_t _pos = src.rfind("%s");
	int matchTime = 0;
	while (_pos != std::string::npos)
	{
		src.replace(_pos, 2, ss[matchTime++]);
		_pos = ssrc.rfind("%s", _pos);
	}*/


}
std::wstring StringToWstring(const char* src)
{
	wchar_t buff[4096];
	memset(buff, 0, sizeof(wchar_t));
	std::wstring s;
	size_t nCvtCnt;
	int erno = mbstowcs_s(&nCvtCnt, buff, 4096, src, 4096);
	s = buff;
	return std::move(buff);
}

std::string wstring2string(std::wstring wstr)
{
	//std::string result;
	////获取缓冲区大小，并申请空间，缓冲区大小事按字节计算的  
	//int len = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), NULL, 0, NULL, NULL);
	//char* buffer = new char[len + 1];
	//memset(buffer, 0, len + 1);
	////宽字节编码转换成多字节编码  
	//int nWCT = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), buffer, len, NULL, NULL);
	//buffer[len] = '\0';
	////删除缓冲区并返回值  
	//result.append(buffer);
	//delete[] buffer;
	const int size = 4096;//wstr.size();
	char* ch = new char[size];
	size_t nCvtCnt;
	//wcstombs_s(&nCvtCnt, ch, wstr.data(), 4096);
	int erno = wcstombs_s(&nCvtCnt, ch, size, wstr.data(), 4096);
	return std::string(ch);
}
int main()
{
	auto vct = ProcessManager::GetAllProcess();
	for (int i = 0; i < vct.size(); i++)
	{
		std::cout << vct[i] << std::endl;
	}
	BOOL b = ProcessManager::IsProcessExits("360");
	std::string bstr = b == TRUE ? "TRUE" : "FALSE";
	std::cout << bstr.data() << std::endl;
	std::string str = "你好啊你好啊你好啊你好啊 滴滴aa";
	//str.replace(str.begin(), str.end(), 'a', '1');
	Replace(str, "你好", "");

	std::string s2 = "adassadsad\r\n asdadada\r\n\r\n\r\n";
	std::string s3 = "adassadsad\r\n asdadaa敖德萨ada";
	std::string trimStr = "ada";
	std::string trimStr2 = "\r\n";
	/*auto it = std::remove_if(s2.begin(), s2.end(), [](char sd) {return sd == 's'; });
	s2.erase(it, s2.end());*/
	Replace(s3, "a", "");
	//std::remove(s3.begin(),s3.end(),'a');
	//Replace(s3, "adaa敖德", "==");
	//Trim(s3, "ada");
	//TrimAll(s2, "\r\n");
	s3.push_back(0);
	auto ps = s2.size() - 1 - s2.find_last_of("\n");//差了一个字符
	int poss2 = s2.size() - s2.rfind("\r\n\r\n");
	int cont = findSubStringCount(s2, "a");

	//rfind 查出来的位置是



	////Trim(s3, trimStr);
	//TrimAll(s2, trimStr2);
	//std::cout << trimStr.size() << std::endl;
	///*
	//Trim(s3, trimStr);*/
	////Replace(s3, "a敖", "f");
	////std::remove(s2)
	//std::cout << s3 << std::endl;
	std::string ssrc = "你好 这次输出字符:%s,%s,%s";

	std::vector<std::string> replStr;
	replStr.push_back("字符1");
	replStr.push_back("字符2");
	replStr.push_back("字符3");
	size_t _pos = ssrc.rfind("%s");
	int matchTime = 0;
	while (_pos != std::string::npos)
	{
		ssrc.replace(_pos, 2, replStr[matchTime++]);
		_pos = ssrc.rfind("%s", _pos);
	}
	std::cout << ssrc << std::endl;

	auto ws = StringToWstring(ssrc.data());
	std::wcout << ws.data() << std::endl;
	auto sws = wstring2string(ws);
	std::cout << sws.data() << std::endl;

	return 0;
}