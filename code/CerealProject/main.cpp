#include <iostream>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/cereal.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/array.hpp>

using namespace std;
class CSobj {
public:
	std::string str;
	int ab;
	float ff;

	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(str, ab, ff);
		//ar(head_, length_, return_frame_type_, timestamp_, frame_type_, data_, check_, trail_);
	}
};
template<typename T>
void serialize(T obj, std::vector<unsigned char> data)
{
	CSobj obj;
	size_t len = sizeof(obj);
	std::vector<unsigned char> retval(len);
	boost::iostreams::stream_buffer<boost::iostreams::array_sink> buf((char*)& retval[0], retval.size());
	std::ostream ss(&buf);
	cereal::BinaryOutputArchive o_archive(ss);
	o_archive(obj);
	data = std::move(retval);
}
template <typename T>
bool deserialize(std::vector<unsigned char> src, T& obj)
{
	if (src.size() <= 0)
	{
		return false;
	}
	boost::iostreams::stream<boost::iostreams::array_source> ss((char*)& src[0], src.size());
	cereal::BinaryInputArchive i_archive(ss);
	i_archive(obj);
	return true;
}

int main()
{
	CSobj obj;
	obj.str = "abc";
	obj.ab = 12;
	obj.ff = 3.14f;
	size_t len = sizeof(obj);
	std::vector<unsigned char> retval(len);
	boost::iostreams::stream_buffer<boost::iostreams::array_sink> buf((char*)& retval[0], retval.size());
	std::ostream ss(&buf);
	cereal::BinaryOutputArchive o_archive(ss);
	o_archive(obj);
	CSobj t;
	deserialize(retval, t);

	return 0;
}