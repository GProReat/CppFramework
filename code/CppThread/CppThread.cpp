﻿// CPPMutex.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <Windows.h>
#include <iostream>
#include <thread>
#include <functional>
#include "CFMutex.h"
#include "CFThread.h"
#include <mutex>
#include <chrono>
using namespace std;

CFMutex mutex_;
//std::mutex mutex_;
int m = 0;
int LoopTest(int m)
{

	/*while (1)
	{
		cout << ".";
		::Sleep(1);
	}*/

	for (int i = 0; i < 1000; i++)
	{
		mutex_.lock();

		//mutex.lock();
		//cout << std::this_thread::get_id() << ":" << m << endl;
		cout << m << endl;
		::this_thread::sleep_for(std::chrono::milliseconds(1));
		m++;
		//mutex_.unlock();
		mutex_.unlock();
	}
	return 0;
}
class B
{
public:
	void LoopTest()
	{

		while (1)
		{
			cout << "-";
			::Sleep(1);
		}
	}
};
//
//typedef void  *(func)();
//
//void Test(func f)
//{
//	f();
//}
//
//void Test(function<void> ff)
//{
//	//ff();
//}
void print(int a, int b)
{
	cout << a << "," << b << endl;
}
class MyThread : public CFThread {
public:
	MyThread() :CFThread()
	{

	}
	virtual ~MyThread() {}
	//void Bind(std::function<void()> fn) {
	//	ff = fn;
	//}
	//void Run()
	//{
	//	CFThread::Run();
	//	//for(int i = 0;i < 10000;i++)
	//	ff();
	//	isRunningState = false;
	//}
private:
	//std::function<void()> ff;
};

int mains()
{
	////参数测试
	//function<void(int, int)> f1 = bind(print, std::placeholders::_1, std::placeholders::_2);
	//function<void(int, int)> f2 = bind(print, 11, 12);
	//function<void()> f3 = bind(print, 33, 42);
	//f1(1,2);
	//f2(3,4);
	//f3();

	//LoopTest();
	//Test(bind(&B::LoopTest, b));
	//std::thread th0(std::bind(&B::LoopTest, b));
	//std::thread th0(LoopTest);
	//th0.joinable() ? std::cout << "true" : std::cout << "false";
	//std::thread th1(LoopTest);
	//std::thread th2(LoopTest);
	//th0.detach();
	/*MyThread* mt = new MyThread();
	mt->Bind(bind(LoopTest, 1));
	((CFThread*)mt)->Detach();
	waitforthread(mt, 3000);*/
	/*th1.detach();
	th2.detach();*/
	//th.join();
	//_Thrd_detach();
	//_Thrd_t tt;
	//_Thrd_create(&tt, (_Thrd_start_t)LoopTest, NULL);
	//_Thrd_detach(tt);

	//while (1)::this_thread::sleep_for(std::chrono::milliseconds(1));
	//while (1)
	//{
	//	//th0.joinable() ? std::cout << "true" : std::cout << "false";
	//	mt->isRunning() ? std::cout << "true" : std::cout << "false";
	//	::this_thread::sleep_for(std::chrono::milliseconds(1));
	//}

	//std::cout << "Hello World!\n"; 

	using namespace std;
	mutex mtx;
	unique_lock<mutex> mlock(mtx);
	condition_variable condv;
	bool bReady = false;
	std::thread th([&]() {
		::Sleep(5000);
		cout << "th" << endl;
		bReady = true;
		condv.notify_one();
	});
	th.detach();
	mtx.lock();
	mtx.unlock();
	//condv.wait(mlock);
	condv.wait_for(mlock, std::chrono::seconds(3));
	cout << "mtx" << endl;
	return 0;
}
