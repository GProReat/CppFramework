#include <iostream>
#include <mutex>
#include <string>
using namespace std;

struct Locker
{
	std::mutex mtx;
	std::unique_lock<std::mutex> lock;
	Locker()
	{
		lock.swap(std::unique_lock<std::mutex>(mtx));
		cout << "locker construct" << endl;
	}
};

Locker lck[4];

std::condition_variable cv;
static void print1()
{
	while (1)
	{
		cv.wait(lck[0].lock);
		cout << "thread 1" << endl;
	}
}
static void print2()
{
	while (1)
	{
		cv.wait(lck[1].lock);
		cout << "thread 2" << endl;
	}
}
static void print3()
{
	while (1)
	{
		cv.wait(lck[2].lock);
		cout << "thread 3" << endl;
	}
}

int main4()
{
	std::thread th1(print1);
	std::thread th2(print2);
	std::thread th3(print3);
	th1.detach();
	th2.detach();
	th3.detach();
	std::string str;

	while (1)
	{
		getline(cin, str);
		if (str == "1")
		{
			cout << "notify one" << endl;
			cv.notify_one();	//一次解锁一个
		}
		if (str == "2")
		{
			cout << "notify all" << endl;
			cv.notify_all();
		}
		if (str == "0")
		{
			break;
		}
	}


	return 0;
}