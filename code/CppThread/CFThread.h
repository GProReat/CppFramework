#pragma once
#include <thread>
#include <Windows.h>
#include <sstream>
using namespace std;

class CFThread
{
public:
	CFThread()
	{
		isRunningState = false;
	}
	~CFThread()
	{
		delete th;
	}
	void Bind(std::function<void()> fn) {
		ff = fn;
	}
	void Detach() {
		th = new std::thread(std::bind(&CFThread::Run, this));
		th->detach();
	}
	void Join() {
		th->join();
	}
	int GetID() {
		int tmpid;
		stringstream ss;
		ss << th->get_id();
		ss >> tmpid;
		return tmpid;
	}
	bool isRunning()
	{
		return isRunningState;
	}
	virtual void Run() {
		isRunningState = true;
		ff();
		isRunningState = false;
	}
	//等我执行完成,第二个参数传递最大时间
	void WaitMe(CFThread* cfthread, int maxtime)
	{
		//
		auto tp = std::chrono::system_clock::now();
		while (true)
		{
			if (!cfthread->isRunning())
			{
				break;
			}
			auto tp1 = std::chrono::system_clock::now();
			auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(tp1 - tp);
			if (duration.count() > maxtime)
			{
				break;
			}
			while (1)::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
	}
	//从全局创建
	/*template<typename t>
	static CFThread Create<t><() {

	}*/
protected:
	std::thread* th;
	volatile bool isRunningState;
	std::function<void()> ff;
};
//等待线程 执行完或者最多等待几秒
void waitforthread(CFThread* cfthread, int maxtime)
{
	//
	auto tp = std::chrono::system_clock::now();
	while (1)
	{
		if (!cfthread->isRunning())
		{
			break;
		}
		auto tp1 = std::chrono::system_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(tp1 - tp);
		if (duration.count() > maxtime)
		{
			break;
		}
	}
}