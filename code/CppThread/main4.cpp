#include <iostream>
#include <mutex>
#include <string>
using namespace std;
static std::mutex mtx;
static std::condition_variable cv;
static int counter_ref = 0;
static void printfunc()
{
	//mtx.lock();
	lock_guard<mutex> lck(mtx);	//等于再构建时候 lock 析构的时候unlock
	cout << counter_ref << endl;
	counter_ref++;
	//mtx.unlock();
}

static void print1()
{
	while (1)
	{
		printfunc();
	}
}

static void print2()
{
	while (1)
	{
		printfunc();
	}
}
static void print3()
{
	while (1)
	{
		printfunc();
	}
}

int main()
{
	std::thread th1(print1);
	std::thread th2(print2);
	std::thread th3(print3);
	th1.detach();
	th2.detach();
	th3.join();

	return 0;
}