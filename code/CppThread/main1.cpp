#include "stdafx.h"
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <deque>
#include <thread>
#include <chrono>

using namespace std;

std::deque<int> dq;

static mutex mtx;
static unique_lock<mutex> lock_(mtx);
static condition_variable cv;
static int generate_data = 0;

static void Product()
{
	while (1)
	{
		dq.push_back(generate_data);
		generate_data++;
		cout << "生产者：" << generate_data << endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
		cv.wait(lock_);
	}
}

static void Consume()
{
	while (1)
	{
		if (dq.size() <= 0)
		{
			cv.notify_one();
			continue;
		}
		int data = dq.front();
		dq.pop_front();
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
		cout << "消费者:" << data << endl;
	}
}


int _tmain1(int argc, _TCHAR* argv[])
{
	//lock_.swap(std::unique_lock<mutex>(mtx));
   // Product();
	std::thread th(Product);
	std::thread th2(Consume);
	th.detach();
	
	th2.join();
	return 0;
}

