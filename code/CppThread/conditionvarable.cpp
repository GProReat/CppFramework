#include  "pch.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <future>
using namespace std;
std::mutex mtx0;
std::unique_lock<std::mutex> lck0(mtx0);
std::mutex mtx1;
std::unique_lock<std::mutex> lck1(mtx1);
std::mutex mtx2;
std::unique_lock<std::mutex> lck2(mtx2);
std::condition_variable var;
const int maxtime = 10;
void func()
{
	for (int i = 0; i < maxtime; i++)
	{
		var.wait(lck0);
		cout << "thread [0]" << i << endl;
	}
}
void func1()
{
	for (int i = 0; i < maxtime; i++)
	{
		var.wait(lck1);
		cout << "thread [1]" << i << endl;
	}
}
void func2()
{
	for (int i = 0; i < maxtime; i++)
	{
		var.wait(lck2);
		cout << "thread [2]" << i << endl;
	}
}


int condv_test()
{
	std::thread th0(func);
	std::thread th1(func1);
	std::thread th2(func2);
	th0.detach();
	th1.detach();
	th2.detach();
	int m;
	while (1)
	{
		cin >> m;
		if (m == 1)
			var.notify_one();
		if (m == 0)
			var.notify_all();

	}
}