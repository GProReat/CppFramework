#include "stdafx.h"
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <deque>
#include <thread>
#include <chrono>

using namespace std;

static std::deque<int> dq;

static mutex mtx;
static unique_lock<mutex> lock_(mtx);
static mutex mtx2;
static unique_lock<mutex> lock_2(mtx2);
static condition_variable cv;
static condition_variable cv2;
static int generate_data = 0;
//生产1个消费一个
static void Product()
{
	while (1)
	{
		dq.push_back(generate_data);
		generate_data++;
		cout << "生产者：" << generate_data << endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
		cv2.notify_one();
		cv.wait(lock_);
	}
}

static void Consume()
{
	while (1)
	{
		cv2.wait(lock_2);
		int data = dq.front();
		dq.pop_front();
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
		cout << "消费者:" << data << endl;
		cv.notify_one();
	}
}


int _tmain2(int argc, _TCHAR* argv[])
{
	//lock_.swap(std::unique_lock<mutex>(mtx));
   // Product();
	std::thread th(Product);
	std::thread th2(Consume);
	th.detach();
	th2.join();
	return 0;
}

