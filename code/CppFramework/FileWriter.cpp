#include<string>
#include<vector>
#include<fstream>
#include "FileWriter.h"

FileWriter::FileWriter()
{
}

FileWriter::~FileWriter()
{
}

void FileWriter::Open(std::string fileName)
{
	fs = new ofstream(fileName.c_str(), ios::binary);
	fs->seekp(0, ios_base::end);
	//streampos pos = fs->tellg();
	//long lSize = static_cast<long>(pos);
	//fs->seekg(0, ios_base::end);
}

void FileWriter::Write(unsigned char * arr, int len)
{
	fs->write((char*)arr, len);
}

void FileWriter::Close()
{
	fs->flush();
	fs->close();
}
