#pragma once
#include "BaseInc.h"
template <typename T>
const std::vector<uint8_t> packed_object(T& obj)
{
	size_t len = sizeof(obj);
	std::vector<unsigned char> retval(len);
	boost::iostreams::stream_buffer<boost::iostreams::array_sink> buf((char*)& retval[0], retval.size());
	std::ostream ss(&buf);
	cereal::BinaryOutputArchive o_archive(ss);
	o_archive(obj);
	return std::move(retval);
}

template <typename T>
bool unpacked_object(const std::vector<uint8_t>& src, T& obj)
{
	if (src.size() <= 0)
	{
		return false;
	}

	boost::iostreams::stream<boost::iostreams::array_source> ss((char*)& src[0], src.size());
	cereal::BinaryInputArchive i_archive(ss);
	i_archive(obj);

	return true;
}
