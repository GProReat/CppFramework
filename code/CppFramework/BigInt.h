#pragma once
#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
using VctInt = std::vector<int>;
class BigInt
{
public:
	static void PrintVctInt(VctInt& vctInt)
	{
		VctInt v;
		v.assign(vctInt.begin(), vctInt.end());
		std::reverse(v.begin(), v
			.end());
		/*for (auto it = vctInt.end(); it != vctInt.begin(); --it)
		{
			std::cout << *it;
		}*/
		for (auto it = v.begin(); it != v.end(); ++it)
		{
			std::cout << *it;
		}
		std::cout << std::endl;
	}
	static void Validate(VctInt& vctInt)
	{
		int m = 0;
		int i = 0;
		while (i < vctInt.size() || m != 0)
		{
			int v = i < vctInt.size() ? vctInt[i] : 0;
			int n = v + m;
			if (i < vctInt.size())
				vctInt[i] = n % 10;
			else
			{
				vctInt.push_back(n % 10);
			}
			m = n / 10;
			i++;
		}
	}
	static void AddPosVal(VctInt &a, int val, int pos)
	{
		if (pos < a.size())
		{
			a[pos] += val;
		}
		else
			a.push_back(val);
		Validate(a);
	}
	static VctInt M1(VctInt& a, VctInt& b)
	{
		VctInt c;
		int cSize = std::max(a.size(), b.size());
		int n;	//每个位相乘的结果
		int nAdd;	//进位数
		int nVal;
		for (int i = 0; i < a.size(); i++)
		{
			for (int j = 0; j < b.size(); j++)
			{
				//两种思路 InsertVectorAtI
				n = a[i] * b[j] * std::pow(10, j);
				BigInt::AddPosVal(c, n, i);
			}
		}
		//c.resize(a.size());
		//c.shrink_to_fit();
		//std::copy(c.begin(), a.begin(), a.end());
		//c.assign(a.begin(), c.end());
		return c;
	}
	static VctInt M2(VctInt& a, VctInt& b)
	{
		VctInt c;
		int cSize = std::max(a.size(), b.size());
		int n;	//每个位相乘的结果
		int nAdd;	//进位数
		int nVal;
		for (int i = 0; i < a.size(); i++)
		{
			for (int j = 0; j < b.size(); j++)
			{
				//算出真实大小然后，Add
				int n = a[i] * b[j] * std::pow(10, i + j);
				// 变成Vector
				BigInt::AddPosVal(c, n, 0);
			}
		}
		return c;
	}
	static VctInt Add(VctInt& a, VctInt& b)
	{
		//存储到res中
		VctInt res;
		int rSize = std::max(a.size(), b.size());
		int n = 0;	//单位相加结果
		bool bAdd = false; //是否进位
		for (int i = 0; i < rSize; i++)
		{
			bool bLessA = i < a.size();
			bool bLessB = i < b.size();
			int av = bLessA ? a[i] : 0;
			int bv = bLessB ? b[i] : 0;
			n = av + bv;
			bool bAdd = n > 10;
			if (res.size() == i)
				res.push_back(bAdd ? n - 10 : n);
			else
				res[i] += bAdd ? n - 10 : n;
			if (bAdd)
			{
				res.push_back(1);
			}
		}
		return res;
	}
	static VctInt Subtract(VctInt& a, VctInt& b)
	{
		VctInt c;
		bool bS1 = false;
		if (a.size() == b.size())
		{
			bS1 = *a.cend() > *b.cend();
		}
		else {
			bS1 = a.size() > b.size();

		}
		VctInt& s1 = bS1 ? a : b;
		VctInt& s2 = !bS1 ? a : b;
		int nSub = 0;	//高位
		for (int i = 0; i < s1.size(); i++)
		{
			int m2 = s2.size() <= i ? 0 : s2[i];
			int t = s1[i] - m2 - nSub;
			if (t < 0)
			{
				nSub = 1;
				t += 10;
				if (s1[i + 1] > 0) {
					nSub--;
					//因为前面已经保证了大数 减 小数
					s1[i + 1]--;
				}
			}
			else {
				nSub = 0;
			}

			c.push_back(t);
		}
		return c;

	}
public:
	BigInt() {}
	BigInt(std::initializer_list<int> lst)
	{
		data.assign(lst);
	}
	BigInt operator =(BigInt b)
	{
		data = b.data;
		return *this;
	}
	BigInt operator +(BigInt& b)
	{
		BigInt bi;
		bi.data = BigInt::Add(data, b.data);
		return bi;
	}
	BigInt operator -(BigInt& b)
	{
		BigInt bi;
		bi.data = BigInt::Subtract(data, b.data);
		return bi;
	}
	BigInt operator *(BigInt& b)
	{
		BigInt bi;
		bi.data = BigInt::M1(data, b.data);
		return bi;
	}

	BigInt Add(BigInt& a, BigInt& b)
	{
		BigInt bi;
		bi.data = BigInt::Add(a.data, b.data);
		return bi;
	}
	void Print()
	{
		BigInt::PrintVctInt(data);
	}
	void Validate() { Validate(data); }
private:
	std::vector<int> data;
};