#pragma once
#include <map>
#include <vector>
#include <deque>
#include <array>
#include <fstream>
#include <functional>
#include <stdio.h>
#include <iostream>
#include <corecrt_io.h>
#include <chrono>
#include <ctime>
#include <time.h>
#include <tchar.h>
#include <iomanip>
#include <iostream>
#include <sstream>
using namespace std;

extern int time_test();
extern void typecast_test();
extern void stringstream_test();
extern void iostream_test();
extern int link_test();