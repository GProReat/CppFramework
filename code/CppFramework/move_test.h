#pragma once
#include "BaseInc.h"
struct A
{
	int a;
	char b;
	~A() {
		std::cout << "析构函数" << endl;
	}
	A() {
		std::cout << "构造函数" << endl;
	}
	void print()
	{
		cout << a << " " << b << endl;
	}
	A(A&& o)
	{
		cout << "移动语义构造函数" << endl;
		a = o.a;
		b = 'b';
	}
};

A&& move_test();
void print(A&& b);