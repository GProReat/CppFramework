#include <stdio.h>
#include <iostream>
#include <corecrt_io.h>
using namespace std;



int main()
{
	std::string fPath = "file.cpp";
	FILE* file = nullptr;
	fopen_s(&file, fPath.data(), "rb");
	if (file)
	{
		int size = _filelength(_fileno(file));
		cout << size << endl;
		fclose(file);
	}
}