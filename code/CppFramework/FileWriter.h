#pragma once
#include "BaseInc.h"
class FileWriter
{
public:
	FileWriter();
	~FileWriter();

	void Open(std::string fileName);
	void Write(unsigned char* arr, int len);
	void Close();
	std::ofstream* fs{ nullptr };
	long fileSize;
private:
	bool isNeedClose{ false };
	bool isFileExists{ false };
};