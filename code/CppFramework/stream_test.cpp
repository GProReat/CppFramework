
#include "BaseInc.h"
using namespace std;

void stringstream_test()
{
	stringstream ss;
	ss << "123 a1 32 13 12";
	string str;
	ss >> hex >> str;
	ss >> oct >> str;
	ss >> dec >> str;
}
void iostream_test()
{

	//十六进制 
	cout << std::hex << 123 << endl;
	//十进制
	cout << std::dec << 123 << endl;
	//八进制
	cout << std::oct << 123 << endl;
	//先是八进制 -》十六进制 ->八进制 输出方式转换
	cout << std::oct << 123 << "," << hex << 123 << "," << dec << 213 << endl;
}