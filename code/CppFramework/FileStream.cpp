#include "FileStream.h"
using namespace std;
FileStream::FileStream()
{
}

FileStream::~FileStream()
{
}

void FileStream::Open(std::string fileName)
{
	fs = new ifstream(fileName.c_str(), ios::binary);
	fs->seekg(0, ios_base::end);
	streampos pos = fs->tellg();
	long lSize = static_cast<long>(pos);
	fs->seekg(0, ios_base::end);
}

void FileStream::Read(unsigned char * arr, int len)
{
	fs->read((char*)arr, len);
}
void FileStream::Close()
{
	fs->close();
}

bool FileStream::IsEnd()
{
	return fs->eof();
}
