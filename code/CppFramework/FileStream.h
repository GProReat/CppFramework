#pragma once
#include "BaseInc.h"
class FileStream
{
public:
	FileStream();
	~FileStream();

	void Open(std::string fileName);
	void Read(unsigned char* arr, int len);
	void Close();
	bool IsEnd();
	std::ifstream* fs{ nullptr };
	long fileSize;
private:
	bool isNeedClose{ false };
	bool isFileExists{ false };

};