#include<string>
#include<string>
#include<vector>
#include<fstream>
#include"FileReader.h"
FileReader::FileReader()
{
}

FileReader::~FileReader()
{
	if (fs != nullptr)
	{
		delete fs;
	}
}

void FileReader::Open(std::string fileName)
{
	fs = new ifstream(fileName.data(), std::ios::in);
	fs->seekg(0, ios_base::end);
	streampos pos = fs->tellg();
	long lSize = static_cast<long>(pos);
	fs->seekg(0, ios_base::end);
}

void FileReader::Read(unsigned char * arr, int len)
{
	fs->read((char*)arr, len);
}

void FileReader::Close()
{
	fs->close();
}

bool FileReader::IsEnd()
{
	return fs->eof();
}
