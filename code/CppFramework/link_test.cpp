// LinkedList.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
#include "BaseInc.h"
#include <iostream>
#include <cstdlib>
using namespace std;
struct LinkNode
{
	int data{ 0 };
	LinkNode* next{ nullptr };
};
//只交换值变换
LinkNode* sortvalue(LinkNode* node)
{
	LinkNode* pHead = node;
	LinkNode* n1Prev = pHead;
	LinkNode* n1 = node->next;
	LinkNode* n2 = n1->next;
	LinkNode* topNode = n1;
	LinkNode* lstTopNode = nullptr;
	while (n2 != lstTopNode)
	{
		//n1 n2地址没变换 所以
		while (n2 != lstTopNode)
		{
			if (n1->data > n2->data)
			{
				int t = n1->data;
				n1->data = n2->data;
				n2->data = t;
			}

			if (n2 == lstTopNode)
			{
				break;
			}
			else
				topNode = n2;

			n1 = n1->next;
			n2 = n2->next;
		}
		lstTopNode = topNode;
		topNode = nullptr;
		n1 = node;
		n2 = n1->next;
	}
	return pHead;
}

//做到了真正的交换节点，不需要交换值
LinkNode* sort(LinkNode* node)
{
	//抽出两个做比较
	LinkNode* pHead = node;
	if (node->next == nullptr)return pHead;
	LinkNode* n1Prev = nullptr;
	LinkNode* n1 = pHead;
	LinkNode* n2 = n1->next;
	LinkNode* topNode = nullptr;
	LinkNode* lstTopNode = nullptr;
	while (n1 != lstTopNode && n2 != lstTopNode)
	{
		while (n1 != lstTopNode && n2 != lstTopNode)
		{
			//冒泡排序 n2属于我冒出来的一个泡泡
			if (n1->data > n2->data)
			{
				topNode = n1;
				if (n1 == pHead)
				{
					pHead = n2;
				}
				//两种排序方式 使用交换地址而不改变值
				//a.如果节点的地址换了那么值不用换 改变地址的话还要考虑上一个节点的走向
				//b.如果值换了那个next的属性不用换
				//n1和n2互换
				LinkNode* tmp = n2->next; //存储n2->next的节点
				n2->next = n1;
				n1->next = tmp;
				if (n1Prev != nullptr)
					n1Prev->next = n2;
				/*n2->data = n1->data;
				n1->next = tmp->next;
				n1->data = n2->data;*/
				//下一次循环 实质为 n2 = n1->next n1 = n2->next
				tmp = n1->next;//tmp
				n1 = n2->next;
				n2 = tmp;

			}
			else {
				topNode = n2;
				n2 = n2->next;
				n1 = n1->next;
			}
			n1Prev = n1Prev == nullptr ? pHead : n1Prev->next;
			//判断是否满足条件

		}
		lstTopNode = topNode;
		topNode = nullptr;
		n1Prev = nullptr;//
		n1 = pHead;
		n2 = n1->next;
	}
	return pHead;
}
void printnode(LinkNode* node)
{
	while (node != nullptr)
	{
		cout << node->data << ",";
		node = node->next;
	}
}

int link_test()
{
	LinkNode* node = new LinkNode();
	node->data = 9;
	node->next = new LinkNode();
	node->next->data = 6;
	LinkNode* ln = node->next;
	for (int i = 0; i < 50; i++)
	{
		ln->next = new LinkNode();
		ln->next->data = rand() % 100;
		ln = ln->next;

	}
	cout << "原始链表:" << endl;
	printnode(node);
	cout << endl;
	//LinkNode* head = sortvalue(node);//sort(node);
	cout << endl;
	LinkNode* head = sort(node);
	cout << "排序后链表:" << endl;
	printnode(head);
	return 0;
}

