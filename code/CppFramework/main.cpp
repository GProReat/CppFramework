#include "BaseInc.h"
#include "FileStream.h"
#include "CFShared_Ptr.h"
//#include "file_test.cpp"
//#include "serialize_test.cpp"
//#include "time_test.cpp"
//#include "tuple_test.cpp"
#include <iostream>

using namespace std;



void ref_test()
{
	CFShared_Ptr<Temp> ptr(new Temp());
	CFShared_Ptr<Temp> pt = ptr;
}

#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

//void ToHex()
//{
//	stringstream ss("AADD2312");
//	unsigned int  tt;
//	/*while (!ss.eof())
//	{
//		ss >> std::hex >> tt >> setw(1);
//		cout << setiosflags(ios::uppercase) << hex << tt << " ";
//	}*/
//	char ch[2];
//	stringstream ss2;
//	while (!ss.eof())
//	{
//		ss.read(ch, 2);
//		//ss >> ch;
//		ss2.write(ch, 2);
//		//ss2.write(&ch, 1);
//		ss2 >> std::hex >> tt;
//		cout << ch[0] << "" << ch[1] << " " << setiosflags(ios::uppercase) << hex << tt << " " << endl;
//		ss2.clear();
//	}
//
//	cout << "Hello World";
//}
//
//std::vector<unsigned char> HexStringToByteArray(std::string src)
//{
//	//有现成的方法
//	////需要去除空格
//	//auto removeIt = std::remove_if(src.begin(), src.end(), [](char ch) {
//	//	return ch == ' ';
//	//});
//	//src.erase(removeIt);
//
//	//stringstream ss("AADD2312");
//	stringstream ss(src);
//	std::vector<unsigned char> vct_byte_arr;
//	unsigned char  byte_data;
//
//	//2个字符串 因为一个字符只能表示0-15
//	char ch[2];
//	stringstream ss2;
//	while (!ss.eof())
//	{
//		ss.read(ch, 2);
//		ss2.write(ch, 2);
//		//ss2.write(&ch, 1);
//		ss2 >> std::hex >> byte_data;	//十六进制转换
//		vct_byte_arr.push_back(byte_data);
//		//打印
//		cout << ch[0] << "" << ch[1] << " " << setiosflags(ios::uppercase) << hex << byte_data << " " << endl;
//		ss2.clear();	//清空流
//		byte_data = 0;
//	}
//	return vct_byte_arr;
//}


int main()
{
	//time_test();
	//HexStringToByteArray("AA DD AD 23 12 ");
	stringstream_test();
	std::string str = "123 432 234";
	//
	/*for (int i = 0; i < str.size(); i++)
	{
		if (str[i]==' ')
		{
			str.erase(str.begin() + i);
			i--;
		}
	}*/
	auto removeIt = std::remove_if(str.begin(), str.end(), [&](char ch) {
		return ch == '2';
		
	});
	str.erase(removeIt);
	str.shrink_to_fit();

	cout << str;

	iostream_test();
	cout << "" << endl;
	link_test();
	getchar();
	return 0;
}