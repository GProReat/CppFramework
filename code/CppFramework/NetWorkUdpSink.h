#pragma once
#include "spdlog/sinks/base_sink.h"
#include "boost/asio.hpp"
template<typename T>
class NetworkUdpSink : spdlog::sinks::base_sink<T>
{
	using socket = boost::asio::ip::udp::socket;
	using io_service = boost::asio::io_service;
	using endpoint = boost::asio::ip::udp::endpoint;
private:
	std::shared_ptr<socket> log_socket;
	std::shared_ptr<endpoint> ep_log_server;	//服务器端口
	//boost::asio::ip::udp::endpoint ep_log_socket;	//客户端端口
	const char* const log_socket_ip = "127.0.0.1";
	const int log_socket_port = 7474;
public:
	//这个作为服务器的ip和端口
	NetworkUdpSink(std::string ip, int port)
	{
		using namespace boost::asio;
		io_service io_service;
		endpoint end_point(ip::address::from_string(log_socket_ip), log_socket_port);

		log_socket = std::make_shared<socket>(io_service);
		//log_socket = std::move(socket(io_service));
		log_socket->bind(end_point);
		ep_log_server = std::make_shared<endpoint>(ip::address::from_string(ip), port);
	}

private:
	void sink_it_(const spdlog::details::log_msg &msg) override
	{
		fmt::memory_buffer formatted;
		using namespace spdlog::sinks;
		sink::formatter_->format(msg, formatted);;
		char* data = formatted.data();
		SendData(data, formatted.size());
	}
	void flush_() override
	{

	}
	void SendData(char* data, int count)
	{
		try
		{
			log_socket->send_to(boost::asio::buffer(data, count), *ep_log_server.get());
		}
		catch (boost::system::system_error &e)
		{
			//std::cout << "发送成功" << e.what() << std::endl;
		}
	}
};