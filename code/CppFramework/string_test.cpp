#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <algorithm>
#include <stdarg.h>
#include <algorithm>
class CppString
{
public:
	CppString(const char* ch)
	{
		val = ch;
	}
	CppString(std::string s)
	{
		val = s;
	}
	void operator= (const char* ch)
	{
		val = ch;
	}
	void operator=(std::string s)
	{
		val = s;
	}
	CppString operator+(const char* ch)
	{
		CppString cpp = std::string(val + ch);
		return cpp;
	}
	CppString operator+(CppString ch)
	{
		CppString cpp = std::string(this->val + ch.val);
		return cpp;
	}
	const char* data() {
		return val.data();
	}
	void Replace(std::string oldStr, std::string newStr)
	{
		size_t pos = val.find(oldStr);
		while (pos != std::string::npos)
		{
			val.replace(pos, oldStr.size(), newStr);
			pos = newStr.find(oldStr, pos + newStr.size());
		}
	}
	void Trim( const std::string& trimStr)
	{
		size_t pos = val.rfind(trimStr);
		if (pos != std::string::npos)
		{
			val.erase(val.begin() + pos - trimStr.size(), val.end());
			val.shrink_to_fit();
		}
	}
	void TrimAll( const std::string& trimStr)
	{
		size_t pos = 0;
		pos = val.rfind(trimStr);
		while (pos == val.size() - trimStr.size())
		{
			val.erase(val.begin() + pos, val.end());
			val.shrink_to_fit();
			//pos = src.find_last_of(trimStr);
			pos = val.rfind(trimStr);
		}
	}
	int findSubStringCount(const std::string&& ss)
	{
		int match = 0;
		size_t pos = val.rfind(ss, 0);
		while (pos != std::string::npos)
		{
			match++;
			pos = val.find(ss, pos + 1);
		}
		return match;
	}
private:
	std::string val;
};
//template <typename T>
//T connect(T arg0, T arg1 ...)
//{
//	va_list   arg_ptr;   //定义可变参数指针 
//	va_start(arg_ptr, arg1);   // i为最后一个固定参数
//	for (;;)
//	{
//		T arg2 = va_arg(arg_ptr, T);   //返回第一个可变参数，类型为int
//	}
//	//T t = connect<T>(arg0, connect(arg1, arg2,arg_ptr));
//	va_end(arg_ptr);        //  清空参数指针
//	return arg0;
//}
template <typename T>
T connect(T arg0, T arg1)
{
	return arg0 + arg1;
}
void remove()
{
	std::string str = "abcdef";
	auto removeIt = std::remove_if(str.begin(), str.end(), [&](char ch) {
		return ch == '2';
	});
	str.erase(removeIt);
	str.shrink_to_fit();
}

void erasetest()
{
	std::string ss = "addefadadgredsa";
	//从第10个字符开始删除到末位
	ss.erase(10);
	//从第二个字符开始删除3个三个字符
	ss.erase(2, 3);
	//迭代器写法
	ss.erase(ss.begin() + 2, ss.begin() + 3);
	//从第八个字符开始写起
	ss.erase(ss.begin() + 8);
}

int string_test()
{
	using namespace std;
	CppString cps = "123";
	CppString cps1 = "b";
	CppString cps2 = "c";
	CppString cps3 = "d";
	cps = "dad";
	//CppString cpps = connect<CppString>(cps, cps1);
	auto cc = cps + cps1 + cps2 + cps3;
	std::cout << cc.data() << std::endl;
	std::string str = "你好132saf";
	std::string childStr = "你好1";
	//返回的是从0开始的顺序 字符串左侧为0 汉字占用2个字节
	auto idx = str.find(childStr);
	//字符串两个字节
	auto pre = str.substr(0, idx);
	auto subed = str.substr(idx, childStr.length());
	auto end = str.substr(idx + childStr.length());
	std::cout << subed.size() << "-" << subed.length() << subed << idx << "," << str[idx] << endl;
	//str.reserve(str.length());
	cout << str << endl << "-------" << endl;
	std::wstring wsr = L"abc你好dass";
	reverse(wsr.rbegin(), wsr.rend());
	locale::global(locale(""));	//locale 传空 按默认
	//setlocale(LC_CTYPE, "");    // MinGW gcc.
	wcout.imbue(locale(""));
	wcout << wsr;
	system("pause");
	return 0;
}


void Replace(std::string& src, const std::string& old, const std::string& newStr)
{
	size_t pos = src.find(old);
	while (pos != std::string::npos)
	{
		src.replace(pos, old.size(), newStr);
		pos = src.find(old, pos + newStr.size());
	}
}
void Trim(std::string& src, const std::string& trimStr)
{
	size_t pos = src.rfind(trimStr);
	if (pos != std::string::npos)
	{
		src.erase(src.begin() + pos - trimStr.size(), src.end());
		src.shrink_to_fit();
	}
}
void TrimAll(std::string& src, const std::string& trimStr)
{
	size_t pos = 0;
	pos = src.rfind(trimStr);
	while (pos == src.size() - trimStr.size())
	{
		src.erase(src.begin() + pos, src.end());
		src.shrink_to_fit();
		//pos = src.find_last_of(trimStr);
		pos = src.rfind(trimStr);
	}
}
int findSubStringCount(std::string& src, const std::string&& ss)
{
	int match = 0;
	size_t pos = src.rfind(ss, 0);
	while (pos != std::string::npos)
	{
		match++;
		pos = src.find(ss, pos + 1);
	}
	return match;
}

std::vector<unsigned char> HexStringToByteArray(std::string src)
{
	using namespace std;
	//有现成的方法
	//需要去除空格
	auto removeIt = std::remove_if(src.begin(), src.end(), [](char ch) {
		return ch == ' ';
	});
	src.erase(removeIt);
	stringstream ss(src);
	std::vector<unsigned char> vct_byte_arr;
	unsigned int byteVal = 0;
	//2个字符串 因为一个字符只能表示0-15
	char ch[2];
	stringstream ss2;
	while (!ss.eof())
	{
		ss.read(ch, 2);
		ss2.write(ch, 2);
		sscanf(ch, "%x", &byteVal);
		vct_byte_arr.push_back(byteVal);
		//打印
		ss2.clear();	//清空流
		byteVal = 0;
	}
	return vct_byte_arr;
}

std::vector<unsigned char>	SToByteArray(std::string& src)
{
	using namespace std;
	stringstream ss(src);
	string tmpstr;
	vector<unsigned char> vct_byte_arr;
	while (!ss.eof())
	{
		unsigned char chVal;
		ss >> tmpstr;
		sscanf(tmpstr.data(), "%x", &chVal);
		vct_byte_arr.push_back(chVal);
	}
	return vct_byte_arr;
}

int string_main()
{
	std::string src = "ff ff ff";
	auto vct = SToByteArray(src);
	return 0;
}