#include <iostream>
#include <vector>
#include <map>
using namespace std;
int tuple_test()
{
	/*char ch = (char)255;
	std::vector<char> vct{ (char)255,(char)255,(char)255,(char)25};
	char*  ch = &vct[0];
	cout << (int)ch[0] << "," << (int)ch[1]<<endl;
	int* pInt = (int*)&vct[0];
	cout << *pInt << endl;*/
	map<int, tuple<int, string, int>> mm;
	mm[12] = make_tuple(12, "abc", 4);
	int a = 0;
	int b = 0;
	string str;
	std::tie(a, str, b) = make_tuple(234, "dsa", 23);

	auto it = mm.find(12);

	std::tie(a, str, b) = it->second;
	cout << a << endl;
	cout << str.data() << endl;
	cout << b << endl;
	return 0;
}