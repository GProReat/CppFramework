#pragma once
#include<iostream>
template <typename T>
class CFShared_Ptr
{
public:
	CFShared_Ptr(T* t)
	{
		this->t = t;
		pRefCount = new int;
		*pRefCount = 1;
	}
	~CFShared_Ptr()
	{
		--*pRefCount;
		if (*pRefCount == 0)
		{
			delete pRefCount;
			delete t;
		}
	}
	CFShared_Ptr<T> operator=(const CFShared_Ptr& obj)
	{
		pRefCount = obj.pRefCount;
		t = obj.t;
		return *this;
	}
	T& get()
	{
		return *t;
	}
	T& operator ->() {
		return t;
	}
private:
	T* t;
	int* pRefCount;
};

class Temp {
public:
	Temp() {
		std::cout << "����" << std::endl;
	}
	~Temp() {
		std::cout << "����" << std::endl;
	}
	int m;
};
