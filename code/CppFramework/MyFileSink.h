#pragma once
#include <string>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/base_sink.h"

template<typename T>
class MyFileSink : spdlog::sinks::base_sink<T>
{
private:
	FILE* file;
public:
	MyFileSink(std::string fileName)
	{
		file == nullptr;
		fopen_s(&file, fileName.c_str(), "wb+");
	}
	static MyFileSink<T> CreateInst(std::string fileName)
	{
		MyFileSink<T> mfs(fileName);
		return std::move(mfs);
	}

private:
	void sink_it_(const spdlog::details::log_msg &msg) override
	{
		fmt::memory_buffer formatted;
		using namespace spdlog::sinks;
		sink::formatter_->format(msg, formatted);
		//file_helper_.write(formatted);
		char* data = formatted.data();
		fwrite(data, sizeof(char), formatted.size(), file);
	}

	void flush_() override
	{
		fflush(file);
	}
};