#pragma once
#include "BaseInc.h"
#include "TcpClient.h"
#include "TcpServer.h"
enum FileColumnAttrib {
	LC_AUTHORITY = 0,
	LC_FileType = 1,
	LC_USER = 2,
	LC_GROUP = 3,
	LC_FILESIZE = 4,
	LC_DATE_MONTH = 5,
	LC_DATE_DAY = 6,
	LC_FILENAME = 7
};
class FTPCient
{
public:
	FTPCient() {
		dataServer.RecvHandler = std::bind(&FTPCient::OnRecvDataChannel, this,
			std::placeholders::_1,std::placeholders::_2,std::placeholders::_3);
	}
	/**
	 * 
	 */
	void Login(string addr, string usr, string pwd);
	/**
	 *服务器的被动模式，下载和上传文件需要被动模式 需要dataclient手动连接
	 */
	void Pasv(); ////
	/**
	 *列出文件列表 包括文件夹和文件
	 */
	void List(); //列出文件列表 包括文件夹和文件
	/**
	 * 主动模式
	 */
	void Port();	
	void Exit();	//退出
	/**
	 * 打印当前目录
	 */
	void Pwd();		
	/**
	 * 改变服务器的工作目录
	 */
	void Cwd();
	/**
	 * 创建目录
	 */
	void MakeDiectory();
	/**
	 * 下载文件,服务器处于被动模式
	 */
	void Retr(string serverFile,string dstFile);
	/**
	 * 上传文件 服务器处于被动模式
	 */
	void Stor(string fileName);
private:
	char sendBuff[SendSize];
	char recvBuff[RecvSize];
	void OnRecvDataChannel(SOCKET,char*,int);
	void SendData(SOCKET);
	string GetPortCmd()
	{
		// 端口是 4 * 256 +2 = 1026
		return "PORT 10,1,70,31,4,2\r\n";

	}
	void ParseListAck(char* buff,int size);
private:
	TcpClient cmdClient;
	TcpClient dataClient;	//被动模式使用
	TcpServer dataServer;	//主动模式使用
	string curCmd;
	string downFileName;
	ofstream ofs;
};